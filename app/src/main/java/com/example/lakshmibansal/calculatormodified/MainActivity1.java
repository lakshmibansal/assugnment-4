package com.example.lakshmibansal.calculatormodified;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static com.example.lakshmibansal.calculatormodified.appConstants.AppConstants.*;

public class MainActivity1 extends Activity {
    EditText first_op, second_op, result;
    String operator;
    LinkedList<String> historyAdd = new LinkedList<>();
    LinkedList<String> historyAll = new LinkedList<>();
    LinkedList<String> historyMul = new LinkedList<>();
    LinkedList<String> historyDiv = new LinkedList<>();
    LinkedList<String> historySub = new LinkedList<>();
    public static Map<String, LinkedList> historyMap = new HashMap<>();
    Double firstOperand, secondOperand, resultValue;
    Button addBtn, minusBtn, mulBtn, divBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        first_op = (EditText) findViewById(R.id.first_operand);
        second_op = (EditText) findViewById(R.id.second_operand);
        result = (EditText) findViewById(R.id.result);
        addBtn = (Button) findViewById(R.id.plusBtn);
        minusBtn = (Button) findViewById(R.id.minusBtn);
        mulBtn = (Button) findViewById(R.id.multiplyBtn);
        divBtn = (Button) findViewById(R.id.divideBtn);

        historyMap.put(ADD, historyAdd);
        historyMap.put(SUB, historySub);
        historyMap.put(MULTIPLY, historyMul);
        historyMap.put(DIVIDE, historyDiv);
        historyMap.put(ALL, historyAll);

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.plusBtn:
                operator = ADD;
                check();
                break;
            case R.id.minusBtn:
                operator = SUB;
                check();
                break;
            case R.id.multiplyBtn:
                operator = MULTIPLY;
                check();
                break;
            case R.id.divideBtn:
                operator = DIVIDE;
                check();
                break;
            case R.id.summaryBtn:
                Intent intent = new Intent(this, SummaryView.class);
                startActivity(intent);
                break;
        }
    }

    public void check() {
        if (first_op.getText().toString().equals("")) {
            Toast.makeText(this, "Enter First Operand", Toast.LENGTH_SHORT).show();
            result.setText("");
        } else if (second_op.getText().toString().equals("")) {
            Toast.makeText(this, "Enter Second Operand", Toast.LENGTH_SHORT).show();
            result.setText("");
        } else {
            firstOperand = Double.parseDouble(first_op.getText().toString());
            secondOperand = Double.parseDouble(second_op.getText().toString());
            if (operator.equals(ADD)) {
                colorChange(addBtn, minusBtn, mulBtn, divBtn);
                resultValue = firstOperand + secondOperand;
                result.setText("" + resultValue);
                historyAdd.addFirst(firstOperand + operator + secondOperand + "=" + resultValue);
                historyAll.addFirst(firstOperand + operator + secondOperand + "=" + resultValue);
            } else if (operator.equals(SUB)) {
                resultValue = firstOperand - secondOperand;
                colorChange(minusBtn, addBtn, mulBtn, divBtn);
                result.setText("" + resultValue);
                historySub.addFirst(firstOperand + operator + secondOperand + "=" + resultValue);
                historyAll.addFirst(firstOperand + operator + secondOperand + "=" + resultValue);
            } else if (operator.equals(MULTIPLY)) {
                resultValue = firstOperand * secondOperand;
                colorChange(mulBtn, addBtn, minusBtn, divBtn);
                result.setText("" + resultValue);
                historyMul.addFirst(firstOperand + operator + secondOperand + "=" + resultValue);
                historyAll.addFirst(firstOperand + operator + secondOperand + "=" + resultValue);

            } else if (operator.equals(DIVIDE)) {
                if (secondOperand != 0) {
                    colorChange(divBtn, addBtn, minusBtn, mulBtn);
                    resultValue = firstOperand / secondOperand;
                    result.setText("" + resultValue);
                    historyAll.addFirst(firstOperand + operator + secondOperand + "=" + resultValue);
                    historyDiv.addFirst(firstOperand + operator + secondOperand + "=" + resultValue);

                } else
                    Toast.makeText(this, "cannot Divide by ZERO", Toast.LENGTH_SHORT).show();
            }

        }
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SHOW_SUMMARY) {
            if (resultCode == RESULT_OK) {
                history = (ArrayList) data.getStringArrayListExtra("history").clone();
            }
        }
    }
*/
    public void colorChange(Button btnChange, Button b1, Button b2, Button b3) {
        btnChange.setBackgroundColor(Color.parseColor(SELECTED_COLOR));
        btnChange.setTextColor(Color.parseColor(WHITE));
        b1.setBackgroundColor(Color.parseColor(UNSELECTED_COLOR));
        b1.setTextColor(Color.parseColor(BLACK));
        b2.setBackgroundColor(Color.parseColor(UNSELECTED_COLOR));
        b2.setTextColor(Color.parseColor(BLACK));
        b3.setBackgroundColor(Color.parseColor(UNSELECTED_COLOR));
        b3.setTextColor(Color.parseColor(BLACK));
    }
}
