package com.example.lakshmibansal.calculatormodified.appConstants;

/**
 * Created by LAKSHMI BANSAL on 1/22/2015.
 */
public interface AppConstants {
    String ADD = " + ";
    String MULTIPLY = " * ";
    String DIVIDE = " / ";
    String SUB = " - ";

    //colors

    String BLACK = "#000000";
    String WHITE = "#ffffff";
    String UNSELECTED_COLOR = "#e6e653";
    String SELECTED_COLOR = "#029456";

    int SHOW_SUMMARY = 100;

    //KEYS
    String BUTTON_KEY = "button";
    String STUDENT_KEY = "student";
    String POSITION_KEY = "position";
    String PREVIOUS_ROLL_KEY = "previous_roll";

    String DIALOG_TITLE = "Options";
    String ALL = "ALL";
    String ADD_STRING = "ADD";
    String MINUS_STRING = "MINUS";
    String MULTIPLY_STRING = "MULTIPLY";
    String DIVIDE_STRING = "DIVIDE";

    final String[] filterItems = {"ALL", "ADD", "MINUS", "MULTIPLY", "DIVIDE"};
}
