package com.example.lakshmibansal.calculatormodified;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LAKSHMI BANSAL on 1/27/2015.
 */
public class SummaryAdapter extends BaseAdapter {
    List<String> history = new ArrayList<>();
    Context context;

    public SummaryAdapter(Context ctx, List<String> list) {
        this.context = ctx;
        this.history = list;
    }

    @Override
    public int getCount() {
        return history.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list_item, null);
        TextView value = (TextView) view.findViewById(R.id.value);
        value.setText(history.get(position));
        return view;
    }

    @Override
    public Object getItem(int position) {
        return history.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
