package com.example.lakshmibansal.calculatormodified;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Collections;

import static com.example.lakshmibansal.calculatormodified.appConstants.AppConstants.*;
import static com.example.lakshmibansal.calculatormodified.MainActivity1.*;

public class SummaryView extends Activity {
    ListView listview;
    Spinner dropdown;
    LinkedList<String> requiredHistory;
    SummaryAdapter adapter;
    Button clearBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary_view);
        listview = (ListView) findViewById(R.id.list);
        dropdown = (Spinner) findViewById(R.id.filter_By);
        clearBtn = (Button) findViewById(R.id.clearBtn);


        ArrayAdapter<String> spinner_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, filterItems);
        dropdown.setAdapter(spinner_adapter);
        requiredHistory = new LinkedList<>();
        requiredHistory = (LinkedList<String>) historyMap.get(ALL).clone();
        //Collections.reverse(requiredHistory);
        adapter = new SummaryAdapter(this, requiredHistory);
        listview.setAdapter(adapter);


        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String choosedItem = (String) parent.getItemAtPosition(position);
                if (choosedItem.equals(ADD_STRING)) {
                    click(ADD);
                } else if (choosedItem.equals(MINUS_STRING)) {
                    click(SUB);
                } else if (choosedItem.equals(MULTIPLY_STRING)) {
                    click(MULTIPLY);
                } else if (choosedItem.equals(DIVIDE_STRING)) {
                    click(DIVIDE);
                } else if (choosedItem.equals(ALL)) {
                    click(ALL);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void onClick(View v) {

        historyMap.get(ALL).clear();
        historyMap.get(ADD).clear();
        historyMap.get(SUB).clear();
        historyMap.get(MULTIPLY).clear();
        historyMap.get(DIVIDE).clear();
        requiredHistory.clear();
        adapter.notifyDataSetChanged();
    }

    public void click(String required) {
        requiredHistory.clear();
        requiredHistory = (LinkedList) historyMap.get(required).clone();
        //Collections.reverse(requiredHistory);
        adapter = new SummaryAdapter(this, requiredHistory);
        listview.setAdapter(adapter);
    }


    /*@Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putStringArrayListExtra("history", history);
        setResult(RESULT_OK, intent);
        finish();
    }*/

}
